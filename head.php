<head>
<meta charset="utf-8">
<title>jQuery validation plug-in - main demo</title>

<!-- http://phpexcel.codeplex.com/ -->


<link rel="stylesheet" href="css/cmxform.css">
<link rel="stylesheet" href="css/cmxformTemplate.css">
<link rel="stylesheet" href="css/core.css">
<link rel="stylesheet" href="css/reset.css">

<link rel="stylesheet" href="css/progress.css">

<script src="js/lib/jquery-1.12.4.min.js"></script>
<script src="js/lib/jquery.validate.js"></script>

<script>
    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#commentForm").validate();

        // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                firstname: "required",
                lastname: "required",
                username: {
                    required: true,
                    minlength: 2
                },
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },
                topic: {
                    required: "#newsletter:checked",
                    minlength: 2
                },
                agree: "required"
            },
            messages: {
                firstname: "Please enter your firstname",
                lastname: "Please enter your lastname",
                username: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 2 characters"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: "Please enter a valid email address",
                agree: "Please accept our policy"
            }
        });

        // propose username by combining first- and lastname
        $("#username").focus(function() {
            var firstname = $("#firstname").val();
            var lastname = $("#lastname").val();
            if (firstname && lastname && !this.value) {
                this.value = firstname + "." + lastname;
            }
        });

        //code to hide topic selection, disable for demo
        var newsletter = $("#newsletter");
        // newsletter topics are optional, hide at first
        var inital = newsletter.is(":checked");
        var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
        var topicInputs = topics.find("input").attr("disabled", !inital);
        // show when newsletter is checked
        newsletter.click(function() {
            topics[this.checked ? "removeClass" : "addClass"]("gray");
            topicInputs.attr("disabled", !this.checked);
        });
    });
    </script>
<style>#commentForm{width:500px;}#commentForm label{width:250px;}#commentForm label.error,#commentForm input.submit{margin-left:253px;}#signupForm{width:670px;}#signupForm label.error{margin-left:10px;width:auto;display:inline;}#newsletter_topics label.error{display:none;margin-left:103px;}</style>
</head>
